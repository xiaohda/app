package com.jello.application.ui.activity;

import android.os.Bundle;
import com.jello.application.R;
import com.jello.application.common.activity.BaseActivity;

public class MainActivity extends BaseActivity { 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
    }

}